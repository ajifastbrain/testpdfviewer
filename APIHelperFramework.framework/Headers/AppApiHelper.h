//
// Created by Tjiputra Yapeter on 09/06/18.
// Copyright (c) 2018 Teravin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppApiHelper : NSObject {
    NSString *accessToken;
}

@property (nonatomic, strong) NSString *accessToken;

+ (instancetype)sharedInstance;
- (NSString *)genXToken;
- (NSString *)generateAuthorizationToken;
- (NSString *)genImei;
- (NSString *)genDeviceID;

- (void)requestHttpGetForResult:(NSString *)apiString withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSData *, NSError *))completion;
- (void)requestHttpGetForResult:(NSString *)apiString withTokenId:(NSString *)tokenId withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSData *, NSError *))completion;
- (void)requestHttpGetForResult:(NSString *)apiString withCompletionBlock:(void (^)(NSData *))completion;
- (void)requestHttpGetForResult:(NSString *)apiString withTokenId:(NSString *)tokenId withOAuth:(NSString *)oAuth withCompletionBlock:(void (^)(NSData *))completion;
- (void)requestHttpPatch:(NSString *)apiString withData:(NSData *)postData withTokenId:(NSString *)tokenId withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSString *, NSError *))completion;
- (void)requestHttpPostForResult:(NSString *)apiString withData:(NSData *)postData withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSData *, NSError *))completion;
- (void)requestHttpPostForResult:(NSString *)apiString withData:(NSData *)postData withTokenId:(NSString *)tokenId withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSData *, NSError *))completion;
- (void)requestHttpPost:(NSString *)apiString withData:(NSData *)postData withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSString *, NSError *))completion;
- (void)requestHttpPost:(NSString *)apiString withData:(NSData *)postData withTokenId:(NSString *)tokenId withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSString *, NSError *))completion;
- (void)requestHttpDelete:(NSString *)apiString withData:(NSData *)postData withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSString *, NSError *))completion;
- (void)requestHttpDelete:(NSString *)apiString withData:(NSData *)postData withTokenId:(NSString *)tokenId withOAuth:(NSString *)oAuth  withCompletion:(void (^)(NSString *, NSError *))completion;
- (void)requestOauthAccessToken:(NSString *)apiString withCompletion:(void (^)(NSString *, NSError *))completion;
- (void)requestHttpPostForResult:(NSString *)apiString withBody:(NSData *)postData withCredential:(NSString *)credential withCompletion:(void (^)(NSData *, NSError *))completion;
- (NSError *)constructError:(NSData *)data;


- (void)requestHttp:(NSString *)apiString withHTTPMethod:(NSString *)method withData:(NSDictionary *)param withTokenId:(NSString *)tokenId withOAuth:(NSString *)oAuth withCompletion:(void (^)(NSData *, NSError *))completion;

//- (void)requestHttp:(NSString *)oAuthToken withUrl:(NSString *)URL withRequest:(NSMutableURLRequest *)request withCompletionBlock:(void (^)(NSData *))completion;
//
//- (void)requestHttp:(NSString *)apiString withRequest:(NSMutableURLRequest *)request withTokenId:(NSString *)tokenId withOAuth:(NSString *)oAuth withCompletionBlock:(void (^)(NSData *))completion;

@end
