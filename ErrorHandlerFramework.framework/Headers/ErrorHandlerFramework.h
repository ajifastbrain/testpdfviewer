//
//  ErrorHandlerFramework.h
//  ErrorHandlerFramework
//
//  Created by Eka Praditya Giovanni Kariim on 17/07/19.
//  Copyright © 2019 Teravin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ErrorHandlerFramework.
FOUNDATION_EXPORT double ErrorHandlerFrameworkVersionNumber;

//! Project version string for ErrorHandlerFramework.
FOUNDATION_EXPORT const unsigned char ErrorHandlerFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ErrorHandlerFramework/PublicHeader.h>

#import <ErrorHandlerFramework/ErrorMessageHandlerService.h>
