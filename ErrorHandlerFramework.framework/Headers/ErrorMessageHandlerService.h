//
//  ErrorMessageHandlerService.h
//  RHBTravelCard
//
//  Created by Eka Praditya Giovanni Kariim on 20/03/19.
//  Copyright © 2019 Teravin Technovations. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ErrorMessageHandlerService : NSObject

-(void)CheckErrorCode:(NSDictionary *)errorDetail WithCompletion:(void(^)(NSDictionary *))value;

-(void)CheckFrameworkRunning;

@end

NS_ASSUME_NONNULL_END
