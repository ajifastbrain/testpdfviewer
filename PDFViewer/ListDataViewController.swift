//
//  ListDataViewController.swift
//  PDFViewer
//
//  Created by Aji Prakosa on 08/05/20.
//  Copyright © 2020 Aji Prakosa. All rights reserved.
//

import UIKit
import APIHelperFramework
import SwiftyJSON

class ListDataViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var mainTable: UITableView!
    private var cellRowArray = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "List Data"
        mainTable.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
        
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.tableFooterView = UIView(frame: CGRect.zero)
        mainTable.separatorStyle = .none
        callApi()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numRows: Int?
        numRows = cellRowArray.count
        return numRows!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ListCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath as IndexPath) as! ListCell
        var post = [String: String]()
        for (key, object) in self.cellRowArray[indexPath.row] {
            post[key] = object.stringValue
        }
        cell.labelTitle.text = post["name"]
        cell.selectionStyle = .default
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var post = [String: String]()
        for (key, object) in self.cellRowArray[indexPath.row] {
            post[key] = object.stringValue
        }
        
        let files: String = post["file"] ?? ""
        let url: URL! = URL(string: files)
        let pdfViewController = PDFViewController(pdfUrl: url)
        self.present(pdfViewController, animated: true, completion: nil)
    }
    

    func callApi() {
        let apihF = AppApiHelper()
        self.showLoadingView()
        apihF.requestHttpGet(forResult: "http://www.mocky.io/v2/5d36642e5600006c003a52c1", withTokenId: apihF.genXToken(), withOAuth: "", withCompletion: {(respJSON, error) -> Void in
            DispatchQueue.main.async {
                if error != nil {
                    print(error as Any)
                    self.hideLoadingView()
                } else {
                    self.hideLoadingView()
                    let jsond = JSON(respJSON as Any)
                    self.cellRowArray = jsond["data"]
                    self.mainTable.reloadData()
                }
            }
        })
    }
    

}
