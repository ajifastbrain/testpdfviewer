//
//  BaseViewController.swift
//  PDFViewer
//
//  Created by Aji Prakosa on 08/05/20.
//  Copyright © 2020 Aji Prakosa. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func showLoadingView() {
        DispatchQueue.main.async {
            if let window = UIApplication.shared.keyWindow {
                LoadingOverlay.shared.showOverlay(inView: window)
            } else {
                LoadingOverlay.shared.showOverlay(inView: self.navigationController!.view)
            }
        }
    }

    func hideLoadingView() {
        DispatchQueue.main.async {
            LoadingOverlay.shared.hideOverlayView()
        }
    }


}
