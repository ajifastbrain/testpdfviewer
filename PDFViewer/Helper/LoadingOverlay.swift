//
//  LoadingOverlay.swift
//  PDFViewer
//
//  Created by Aji Prakosa on 08/05/20.
//  Copyright © 2020 Aji Prakosa. All rights reserved.
//

import Foundation
import UIKit


public class LoadingOverlay {
    var overlayView = UIView()
    var indicatorActivity = UIActivityIndicatorView()
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }

    public func showOverlay(inView view: UIView) {
        if !overlayView.isDescendant(of: view) {
            overlayView = UIView(frame: UIScreen.main.bounds)
            overlayView.backgroundColor = UIColor.clear
            
            let imageViewbg = UIView()
            imageViewbg.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
            imageViewbg.layer.cornerRadius = 10
            imageViewbg.backgroundColor = UIColor.init(hexString: "#1C7ED6")
            imageViewbg.alpha = 0.8
            imageViewbg.center = overlayView.center
            
           
            indicatorActivity.frame = CGRect(x: 0, y: 0, width: 50.0, height: 50.0)
            indicatorActivity.style = .whiteLarge
            indicatorActivity.color = UIColor.white
            indicatorActivity.startAnimating()
            indicatorActivity.center = overlayView.center
            
            overlayView.addSubview(imageViewbg)
            overlayView.addSubview(indicatorActivity)
            
            view.isUserInteractionEnabled = false
            view.addSubview(overlayView)
            view.fadeIn()
        }
    }
    
    public func hideOverlayView() {
        overlayView.superview?.isUserInteractionEnabled = true
        overlayView.removeFromSuperview()
        overlayView.fadeOut()
    }
    
}
