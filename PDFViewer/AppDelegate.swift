//
//  AppDelegate.swift
//  PDFViewer
//
//  Created by Aji Prakosa on 08/05/20.
//  Copyright © 2020 Aji Prakosa. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let rootVC = ListDataViewController() // your custom viewController. You can instantiate using nib too. UIViewController(nib name, bundle)
        
        let navController = BaseNavigationController(rootViewController: rootVC) // Integrate navigation controller programmatically if you want
        window?.rootViewController = navController

        return true
    }

    
}

