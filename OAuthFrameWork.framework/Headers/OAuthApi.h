//
//  OAuthApi.h
//  RHBTravelCard
//
//  Created by Tjiputra Yapeter on 08/09/18.
//  Copyright © 2018 Teravin Technovations. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OAuthApi : NSObject

+ (instancetype)sharedInstance;
- (void)requestOauthAccessToken:(NSString *)apiString withAuth:(NSString *)authBasic withData:(NSDictionary *)dataBody withCompletion:(void (^)(NSString *, NSDictionary *))completion;
- (void)requestOauthAccessData:(NSString *)apiString withAuth:(NSString *)authBasic withData:(NSDictionary *)dataBody withCompletion:(void (^)(NSData *, NSDictionary *))completion;
//- (void)requestPasswordOauthToken:(NSString *)userId withPassword:(NSString *)password withDeviceId:(NSString *)deviceId withCompletion:(void (^)(NSDictionary *, NSDictionary *))completion;
//- (void)requestPinOauthToken:(NSString *)pin withImei:(NSString *)imei
//                withDeviceId:(NSString *)deviceId withCompletion:(void (^)(NSString *, NSDictionary *,NSError *))completion;

@end
