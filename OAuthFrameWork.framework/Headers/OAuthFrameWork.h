//
//  OAuthFrameWork.h
//  OAuthFrameWork
//
//  Created by Eka Praditya Giovanni Kariim on 24/07/19.
//  Copyright © 2019 Teravin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for OAuthFrameWork.
FOUNDATION_EXPORT double OAuthFrameWorkVersionNumber;

//! Project version string for OAuthFrameWork.
FOUNDATION_EXPORT const unsigned char OAuthFrameWorkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OAuthFrameWork/PublicHeader.h>

#import <OAuthFrameWork/OAuthApi.h>

